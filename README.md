# Introspect Data Helm Chart Repository

Please note - these are meant to be used as examples for specific usecases. In general, we don't advise using these charts as-is for most environments, but figured they were worth sharing given the amount of effort we put into it!

## Prerequisites

*   [kubectl](https://kubernetes.io/docs/tasks/tools/install-kubectl/) installed
*   [helm](https://docs.helm.sh/using_helm/#installing-helm) installed (requires `kubectl`)
*   Your local kubeconfig set up to talk to your K8s cluster
*   Our local helm repo needs to be installed (hosted on in this repo!)

```bash
helm repo add id-public  "https://introspectdata.gitlab.io/public/helm-charts"
```
## Helpful Reading

*   [Using Helm](https://github.com/kubernetes/helm/blob/master/docs/using_helm.md)

## CI Workflow

This project has a bit of an 'odd' CI workflow through CircleCI:

*   Only commits to master trigger builds
*   Within that build, each individual chart needs to be specified in the build configuration
   *   If you're adding a new chart--make sure you update the build!
*   This specific build profile has it's SSH deploy key set to read/write - so once it lints, packages and re-indexes the helm repo, it pushes to github again
    *   **VERY IMPORTANT** - the comment of the commit from CircleCI has a [`[skip ci]`](https://circleci.com/docs/2.0/skip-build/) directive that makes it so that it doesn't loop infinitely.

## Useful Commands

### Rebuilding the Helm Repo Index

```bash
helm repo index . --merge index.yaml
```

### Install Helm!

```bash
wget https://storage.googleapis.com/kubernetes-helm/helm-v2.9.1-linux-amd64.tar.gz -O /tmp/helm.tar.gz
sudo tar xvf /tmp/helm.tar.gz -C /usr/local/bin/ --strip 1
rm -rf /tmp/helm.tar.gz
```
