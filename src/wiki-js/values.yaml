# Default values for wiki-js.
# This is a YAML-formatted file.
# Declare variables to be passed into your templates.

replicaCount: 1

image:
  repository: requarks/wiki
  tag: latest
  pullPolicy: IfNotPresent

nameOverride: ""
fullnameOverride: ""

service:
  type: ClusterIP
  port: 80

ingress:
  enabled: true
  annotations:
     kubernetes.io/ingress.class: nginx-ops
     kubernetes.io/tls-acme: "true"
  paths:
  hosts:
    - wiki.ops.introspectdata.com
  tls:
    - secretName: wiki-ops-introspectdata-com-tls
      hosts:
        - wiki.ops.introspectdata.com

config:
  wikijs-conf:
    #######################################################################
    # Wiki.js - CONFIGURATION                                             #
    #######################################################################
    # Full explanation + examples in the documentation:
    # https://docs.requarks.io/wiki/install
    # You can use an ENV variable by using $(ENV_VAR_NAME) as the value
    # ---------------------------------------------------------------------
    # Title of this site
    # ---------------------------------------------------------------------
    title: Wiki
    # ---------------------------------------------------------------------
    # Full public path to the site, without the trailing slash
    # ---------------------------------------------------------------------
    # INCLUDE CLIENT PORT IF NOT 80/443!
    host: http://wiki.ops.introspectdata.com
    # ---------------------------------------------------------------------
    # Port the main server should listen to (80 by default)
    # ---------------------------------------------------------------------
    # To use process.env.PORT, comment the line below:
    port: 80
    # ---------------------------------------------------------------------
    # Data Directories
    # ---------------------------------------------------------------------
    paths:
      repo: ./repo
      data: ./data
    # ---------------------------------------------------------------------
    # Upload Limits
    # ---------------------------------------------------------------------
    # In megabytes (MB)
    uploads:
      maxImageFileSize: 3
      maxOtherFileSize: 100
    # ---------------------------------------------------------------------
    # Site Language
    # ---------------------------------------------------------------------
    # Possible values: en, de, es, fa, fr, ja, ko, nl, pt, ru, sr, sv, tr or zh
    lang: en
    # Enable for right to left languages (e.g. arabic):
    langRtl: false
    # ---------------------------------------------------------------------
    # Site Authentication
    # ---------------------------------------------------------------------
    public: false
    auth:
      defaultReadAccess: false
      local:
        enabled: true
      google:
        enabled: true
        clientId: GOOGLE_CLIENT_ID
        clientSecret: GOOGLE_CLIENT_SECRET
      microsoft:
        enabled: true
        clientId: MS_APP_ID
        clientSecret: MS_APP_SECRET
      facebook:
        enabled: false
        clientId: FACEBOOK_APP_ID
        clientSecret: FACEBOOK_APP_SECRET
      github:
        enabled: false
        clientId: GITHUB_CLIENT_ID
        clientSecret: GITHUB_CLIENT_SECRET
      slack:
        enabled: false
        clientId: 'SLACK_CLIENT_ID'
        clientSecret: 'SLACK_CLIENT_SECRET'
      ldap:
        enabled: false
        url: ldap://serverhost:389
        bindDn: cn='root'
        bindCredentials: BIND_PASSWORD
        searchBase: o=users,o=example.com
        searchFilter: (uid={{username}})
        tlsEnabled: false
        tlsCertPath: C:\example\root_ca_cert.crt
      azure:
        enabled: false
        clientId: APP_ID
        clientSecret: APP_SECRET_KEY
        resource: '00000002-0000-0000-c000-000000000000'
        tenant: 'YOUR_TENANT.onmicrosoft.com'
      oauth2:
        enabled: false
        clientId: OAUTH2_CLIENT_ID
        clientSecret: OAUTH2_CLIENT_SECRET
        authorizationURL: OAUTH2_AUTH_URL
        tokenURL: OAUTH2_TOKEN_URL
      oidc:
        enabled: false
        clientId: OPENID_CONNECT_CLIENT_ID
        clientSecret: OPENID_CONNECT_CLIENT_SECRET
        issuer: OPENID_CONNECT_ISSUER
        userInfoUrl: OPENID_CONNECT_USER_INFO_URL
        authorizationURL: OPENID_CONNECT_AUTHORIZATION_URL
        tokenURL: OPENID_CONNECT_TOKEN_URL
        emailClaim: OPENID_CONNECT_EMAIL_CLAIM_PATH
        usernameClaim: OPENID_CONNECT_USERNAME_CLAIM_PATH
    # ---------------------------------------------------------------------
    # Secret key to use when encrypting sessions
    # ---------------------------------------------------------------------
    # Use a long and unique random string (256-bit keys are perfect!)
    sessionSecret: 1234567890abcdefghijklmnopqrstuvxyz

    # ---------------------------------------------------------------------
    # Database Connection String
    # ---------------------------------------------------------------------
    db: mongodb://localhost:27017/wiki
    # ---------------------------------------------------------------------
    # Git Connection Info
    # ---------------------------------------------------------------------
    git:
      url: https://github.com/Organization/Repo
      branch: master
      auth:
        # Type: basic or ssh
        type: ssh
        # Only for Basic authentication:
        username: marty
        password: MartyMcFly88

        # Only for SSH authentication:
        privateKey: /etc/wiki/keys/git.pem

        sslVerify: true

      # Default email to use as commit author
      serverEmail: marty@example.com

      # Whether to use user email as author in commits
      showUserEmail: true
    # ---------------------------------------------------------------------
    # Features
    # ---------------------------------------------------------------------
    # You can enable / disable specific features below
    features:
      linebreaks: true
      mathjax: true
    # ---------------------------------------------------------------------
    # External Logging
    # ---------------------------------------------------------------------
    externalLogging:
      bugsnag: false
      loggly: false
      papertrail: false
      rollbar: false
      sentry: false
    # ---------------------------------------------------------------------
    # Color Theme
    # ---------------------------------------------------------------------
    theme:
      primary: indigo
      alt: blue-grey
      viewSource: all # all | write | false
      footer: blue-grey
      code:
        dark: true
        colorize: true


resources: {}
  # We usually recommend not to specify default resources and to leave this as a conscious
  # choice for the user. This also increases chances charts run on environments with little
  # resources, such as Minikube. If you do want to specify resources, uncomment the following
  # lines, adjust them as necessary, and remove the curly braces after 'resources:'.
  # limits:
  #  cpu: 100m
  #  memory: 128Mi
  # requests:
  #  cpu: 100m
  #  memory: 128Mi

nodeSelector: {}

tolerations: []

affinity: {}


mongodb:
  ## Global Docker image registry
## Please, note that this will override the image registry for all the images, including dependencies, configured to use the global value
##
# global:
#   imageRegistry:

image:
  ## Bitnami MongoDB registry
  ##
  registry: docker.io
  ## Bitnami MongoDB image name
  ##
  repository: bitnami/mongodb
  ## Bitnami MongoDB image tag
  ## ref: https://hub.docker.com/r/bitnami/mongodb/tags/
  ##
  tag: 4.0.3

  ## Specify a imagePullPolicy
  ## ref: http://kubernetes.io/docs/user-guide/images/#pre-pulling-images
  ##
  pullPolicy: Always
  ## Optionally specify an array of imagePullSecrets.
  ## Secrets must be manually created in the namespace.
  ## ref: https://kubernetes.io/docs/tasks/configure-pod-container/pull-image-private-registry/
  ##
  # pullSecrets:
  #   - myRegistrKeySecretName

## Enable authentication
## ref: https://docs.mongodb.com/manual/tutorial/enable-authentication/
#
usePassword: true
# existingSecret: name-of-existing-secret

## MongoDB admin password
## ref: https://github.com/bitnami/bitnami-docker-mongodb/blob/master/README.md#setting-the-root-password-on-first-run
##
# mongodbRootPassword:

## MongoDB custom user and database
## ref: https://github.com/bitnami/bitnami-docker-mongodb/blob/master/README.md#creating-a-user-and-database-on-first-run
##
# mongodbUsername: username
# mongodbPassword: password
# mongodbDatabase: database


## Whether enable/disable IPv6 on MongoDB
## ref: https://github.com/bitnami/bitnami-docker-mongodb/blob/master/README.md#enabling/disabling-ipv6
##
mongodbEnableIPv6: true

## MongoDB additional command line flags
##
## Can be used to specify command line flags, for example:
##
## mongodbExtraFlags:
##  - "--wiredTigerCacheSizeGB=2"
mongodbExtraFlags: []

## Pod Security Context
## ref: https://kubernetes.io/docs/tasks/configure-pod-container/security-context/
##
securityContext:
  enabled: true
  fsGroup: 1001
  runAsUser: 1001

## Kubernetes Cluster Domain
clusterDomain: cluster.local

## Kubernetes service type
service:
  annotations: {}
  type: ClusterIP
  # clusterIP: None
  port: 27017

  ## Specify the nodePort value for the LoadBalancer and NodePort service types.
  ## ref: https://kubernetes.io/docs/concepts/services-networking/service/#type-nodeport
  ##
  # nodePort:

## Setting up replication
## ref: https://github.com/bitnami/bitnami-docker-mongodb#setting-up-a-replication
#
replicaSet:
  ## Whether to create a MongoDB replica set for high availability or not
  enabled: false
  useHostnames: true

  ## Name of the replica set
  ##
  name: rs0

  ## Key used for replica set authentication
  ##
  # key: key

  ## Number of replicas per each node type
  ##
  replicas:
    secondary: 1
    arbiter: 1
  ## Pod Disruption Budget
  ## ref: https://kubernetes.io/docs/concepts/workloads/pods/disruptions/
  pdb:
    minAvailable:
      primary: 1
      secondary: 1
      arbiter: 1

# Annotations to be added to MongoDB pods
podAnnotations: {}

# Additional pod labels to apply
podLabels: {}

## Configure resource requests and limits
## ref: http://kubernetes.io/docs/user-guide/compute-resources/
##
resources: {}
# limits:
#   cpu: 500m
#   memory: 512Mi
# requests:
#   cpu: 100m
#   memory: 256Mi

## Node selector
## ref: https://kubernetes.io/docs/concepts/configuration/assign-pod-node/#nodeselector
nodeSelector: {}

## Affinity
## ref: https://kubernetes.io/docs/concepts/configuration/assign-pod-node/#affinity-and-anti-affinity
affinity: {}

## Tolerations
## ref: https://kubernetes.io/docs/concepts/configuration/taint-and-toleration/
tolerations: []

## Enable persistence using Persistent Volume Claims
## ref: http://kubernetes.io/docs/user-guide/persistent-volumes/
##
persistence:
  enabled: true
  ## A manually managed Persistent Volume and Claim
  ## Requires persistence.enabled: true
  ## If defined, PVC must be created manually before volume will be bound
  # existingClaim:

  ## mongodb data Persistent Volume Storage Class
  ## If defined, storageClassName: <storageClass>
  ## If set to "-", storageClassName: "", which disables dynamic provisioning
  ## If undefined (the default) or set to null, no storageClassName spec is
  ##   set, choosing the default provisioner.  (gp2 on AWS, standard on
  ##   GKE, AWS & OpenStack)
  ##
  # storageClass: "-"
  accessModes:
    - ReadWriteOnce
  size: 8Gi
  annotations: {}

## Configure extra options for liveness and readiness probes
## ref: https://kubernetes.io/docs/tasks/configure-pod-container/configure-liveness-readiness-probes/#configure-probes)
livenessProbe:
  enabled: true
  initialDelaySeconds: 30
  periodSeconds: 10
  timeoutSeconds: 5
  failureThreshold: 6
  successThreshold: 1
readinessProbe:
  enabled: true
  initialDelaySeconds: 5
  periodSeconds: 10
  timeoutSeconds: 5
  failureThreshold: 6
  successThreshold: 1

# Entries for the MongoDB config file
configmap:
#  # Where and how to store data.
#  storage:
#    dbPath: /opt/bitnami/mongodb/data/db
#    journal:
#      enabled: true
#    #engine:
#    #wiredTiger:
#  # where to write logging data.
#  systemLog:
#    destination: file
#    logAppend: true
#    path: /opt/bitnami/mongodb/logs/mongodb.log
#  # network interfaces
#  net:
#    port: 27017
#    bindIp: 0.0.0.0
#    unixDomainSocket:
#      enabled: true
#      pathPrefix: /opt/bitnami/mongodb/tmp
#  # replica set options
#  #replication:
#  #  replSetName: replicaset
#  # process management options
#  processManagement:
#     fork: false
#     pidFilePath: /opt/bitnami/mongodb/tmp/mongodb.pid
#  # set parameter options
#  setParameter:
#     enableLocalhostAuthBypass: true
#  # security options
#  security:
#    authorization: enabled
#    #keyFile: /opt/bitnami/mongodb/conf/keyfile

## Prometheus Exporter / Metrics
##
metrics:
  enabled: false

  image:
    registry: docker.io
    repository: forekshub/percona-mongodb-exporter
    tag: latest
    pullPolicy: IfNotPresent
    ## Optionally specify an array of imagePullSecrets.
    ## Secrets must be manually created in the namespace.
    ## ref: https://kubernetes.io/docs/tasks/configure-pod-container/pull-image-private-registry/
    ##
    # pullSecrets:
    #   - myRegistrKeySecretName

  ## Metrics exporter resource requests and limits
  ## ref: http://kubernetes.io/docs/user-guide/compute-resources/
  ##
  # resources: {}

  ## Metrics exporter pod Annotation
  podAnnotations:
    prometheus.io/scrape: "true"
    prometheus.io/port: "9216"
  serviceMonitor:
    enabled: false
    additionalLabels: {}
    alerting:
      rules: {}
      additionalLabels: {}
